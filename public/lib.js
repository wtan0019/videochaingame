var countT;
var submitTime;


class Socket {
    constructor(ip, port) {
        this.ip = ip;
        this.port = port;
        this.rooms = [];
        this.peerPlayers = {};
        this.playerID = null;
        this.chainPos = null;
        this.link = [];
        this.chainName = null;
        this.joined = null;
        this.sockID = null;
        this.ready = false; // condition when click the button
        this.subReady = false;


        this.socket = io.connect();

        this.socket.on('created', (room, socketId) => {
            trace(`${socketId} successfully created ${room}.`);
            socketIdElem.innerHTML = this.socket.id;
            this.rooms.push(room);
            this.hostID = this.socket.id;
        });

        this.socket.on('joined', (joinPos, socketId, urlLR) => {
            this.sockID = socketId;
            this.playerID = joinPos.playerID;
            this.chainName = joinPos.chainName;
            this.chainPos = joinPos.chainPos;
            this.joined = true;
            this.link = {"left": "https://www.talkroom.io/call/" + urlLR.left,
                        "right": "https://www.talkroom.io/call/" + urlLR.right}
        });

       

        this.socket.on('geturl', (urlLR, firstAllowPos) => {
            console.log("urlLR", urlLR)
            var urlL;
            var urlR;
            if (firstAllowPos === false) {
                document.getElementById("leftscreen").setAttribute("src", urlLR);
                document.getElementById("rightscreen").setAttribute("src", urlLR);
                console.log("fefefefe")
                document.getElementById("leftID").innerHTML = "SCREEN1"
                console.log("fefefefe")
                document.getElementById("rightID").innerHTML = "SCREEN2"
                document.getElementById("selfID").innerHTML = "The game is full, please ask the admin!"
                

                var readyBut = document.getElementById('button')
                readyBut.style.display = 'none'

                this.socket.disconnect();

            } else {
                if (urlLR.left === false){
                    urlL = "https://www.gstatic.com/webp/gallery3/2.png";
                    document.getElementById("leftID").innerHTML = "No player";
                } else {
                    urlL = "https://www.talkroom.io/call/"+String(urlLR.left)
                    document.getElementById("leftID").innerHTML = "Player " + String(firstAllowPos.chainPos - 1)
                }
                if (urlLR.right === false){
                    urlR = "https://www.gstatic.com/webp/gallery3/2.png";
                    document.getElementById("rightID").innerHTML = "No player";
                } else {
                    urlR = "https://www.talkroom.io/call/"+String(urlLR.right)
                    document.getElementById("rightID").innerHTML = "Player " + String(firstAllowPos.chainPos + 1)
                }
                
                document.getElementById("leftscreen").setAttribute("src", urlL);
                document.getElementById("rightscreen").setAttribute("src", urlR);

                // console.log("firstAllowPos",firstAllowPos.chainPos)
                // console.log("dsfdsfdsfdsfs", document.getElementById("selfID").innerHTML)
                // document.getElementById("selfID").innerHTML = "You are the player "+String(firstAllowPos.chainPos) 
                // + " in the position " + String(firstAllowPos.chainPos) + " of chain " + String(firstAllowPos.chainName);
                document.getElementById("selfID").innerHTML = "Chain " + String(firstAllowPos.chainName) + " Player "
                + String(firstAllowPos.chainPos);
            }

        });

        this.socket.on('restart', (lenOfChain) => {
            document.getElementById("leftscreen").setAttribute("src", "https://www.gstatic.com/webp/gallery3/2.png");
            document.getElementById("rightscreen").setAttribute("src", "https://www.gstatic.com/webp/gallery3/2.png");
            document.getElementById("selfID").innerHTML = "Close your browser and rejoin the game!"
            document.getElementById("leftID").innerHTML = "SCREEN1"
            document.getElementById("rightID").innerHTML = "SCREEN2"

            resetReadyBut();
            document.getElementById("clock").innerHTML = 'PAGE EXPIRED'
            clearInterval(countT);
            document.getElementById('submit').style.display = "none";

            for (var i = 1;i<= lenOfChain;i++){
                setCircles("circle" + String(i), 10, "#A9A9A9", 0.5);
                setTexts("text" + String(i), "12px", "normal", 0.3);
            }
            console.log("in disconnect")
            this.socket.disconnect();
        });


        this.socket.on('updateTextCounT', () => {
            document.getElementById("clock").innerHTML = "";
            clearInterval(countT)

        });


        this.socket.on('drawChain', (chaincircleWid, lenOfChain, cx, cy, r, color) => {
            var circles, circleID;
            var svg1 = document.getElementById('chaincircle');
            svg1.setAttribute("width", chaincircleWid);

            for (var i=1;i<=lenOfChain;i++){
                var updateCx = cx + (i-1) * 33;
                circles = document.createElementNS("http://www.w3.org/2000/svg", "circle");
                circleID = "circle" + String(i);
                circles.setAttribute("id",circleID);
                circles.setAttribute("cx",updateCx);
                circles.setAttribute("cy",cy);
                circles.setAttribute("r",r);
                circles.setAttribute("fill", color);
                circles.setAttribute("fill-opacity", 0.5);
                svg1.appendChild(circles);

                var myTextElement = document.createElementNS("http://www.w3.org/2000/svg", "text");
                var myText = document.createTextNode("P"+String(i));
                myTextElement.setAttribute("id","text"+String(i));
                myTextElement.setAttribute("x", updateCx);
                myTextElement.setAttribute("y", cy+5);
                myTextElement.setAttribute("text-anchor", "middle");
                myTextElement.setAttribute("fill", "black");
                myTextElement.setAttribute("font-family", "Arial");
                myTextElement.setAttribute("fill-opacity", 0.3);
                myTextElement.setAttribute("font-size", "12px");
                myTextElement.appendChild(myText);
                svg1.appendChild(myTextElement)
               
            }
            
        });


        this.socket.on('updateCirclePos', (firstAllowPos, color, r) => {
            // firstAllowPos = {"chainName": i,"playerID" : k, "chainPos":j}
            var circleID;
            circleID = "circle" + String(firstAllowPos.chainPos)
            setCircles(circleID, r, color, 1)

            var textID;
            textID = "text" + String(firstAllowPos.chainPos)
            setTexts(textID, "13px", "bold", 1)
 
        });


        this.socket.on('updateCircleOn', (joinedPos) => {
            // console.log(joinedPos)
            for (var i=0;i<joinedPos.length;i++){
                var posID = joinedPos[i];
                // console.log("for joinedPos",joinedPos)
                // console.log("for posID", posID)
                if (posID != String(this.chainPos)){
                    setCircles("circle" + String(posID), 10, "white", 1)
                    setTexts("text" + String(posID), "12px", "normal", 1)
                } 
                
            }

        });

        this.socket.on('updateCircleOff', (disPos, playerOnlineIDs) => {
            var pid = disPos.chainPos;
            setCircles("circle" + String(pid), 10, "#A9A9A9", 0.5);
            setTexts("text" + String(pid), "12px", "normal", 0.3);

            for (var i=0;i<playerOnlineIDs.length;i++){
                console.log(playerOnlineIDs[i])
                resetCircles(playerOnlineIDs[i], this.playerID)
            }

            // 
            
        });

        this.socket.on('updateReadyButOff', (readyButState) => {
            this.ready = readyButState;
            resetReadyBut()
        });

        function resetReadyBut(){
            var readyBut = document.getElementById('button')
            readyBut.innerHTML = 'Waiting for all players in chain';
            readyBut.style.backgroundColor = 'white'
            readyBut.style.color = 'black'
            readyBut.style.width = '230px'
            readyBut.style.boxShadow = '0 3px #999'
            readyBut.disabled = true;
        }

        function resetCircles(posID, playerID){
            var lastCharID = posID.charAt(posID.length -1)
            if (posID === playerID){
                setCircles("circle" + String(lastCharID), 13, "white", 1);
                setTexts("text" + String(lastCharID), "13px", "bold", 1)
            } else {
                setCircles("circle" + String(lastCharID), 10, "white", 1);
                setTexts("text" + String(lastCharID), "12px", "normal", 1)
            }
        }


        function setCircles(circleID, r, color, opacity){
            var circle;
            console.log(circleID)
            console.log("r: "+ r)
            circle = document.getElementById(circleID);
            circle.setAttribute("r",r);
            circle.setAttribute("fill",color);
            circle.setAttribute("fill-opacity", opacity);
        }
        

        function setTexts(textID, size, weight, opacity){
            var text;
            text = document.getElementById(textID);
            text.setAttribute("fill-opacity", opacity);
            text.setAttribute("font-weight", weight);
            text.setAttribute("font-size", size);
        }

        // this.socket.on('terminate', function (){
        //     socket.disconnect();
        // });



        this.socket.on('updateRBut', () => {
            // var ready = document.getElementById("button");
            // console.log("updateRBut b4")
            // console.log(ready)
            // ready.setAttribute("background-color",colorRBut);

            document.getElementById('button').addEventListener('click',  () => {
                var readyBut = document.getElementById('button')
                // var readyButText = document.getElementById('readyButText')
				if (this.ready === false) {
                    readyBut.style.backgroundColor = 'red'
                    readyBut.innerHTML = 'Cancel'
                    console.log("false")
                    console.log(readyBut)
                    this.ready = true
                    clearInterval(countT)
             
                    document.getElementById("clock").innerHTML = "Wait for other players ready.";
                    this.socket.emit('updatingCirCor', this.ready, this.chainPos, this.playerID, this.chainName);
                    this.socket.emit('checkChainReadyState', this.chainName);
                    
              
                } else {
                    clearInterval(countT)
                    readyBut.style.backgroundColor = 'green'
                    readyBut.innerHTML = 'Ready'
                    this.ready = false
                    setupCountdown(this.socket);
                    this.socket.emit('updatingCirCor', this.ready, this.chainPos, this.playerID, this.chainName);
                    console.log("true")
                    console.log(readyBut)
                    
                }
				
			});
            
        });


        this.socket.on('updatedCirCor', (ready, upStatePos) => {
            // change to green color for all online players
            var circleID = "circle" + String(upStatePos);
            var circle = document.getElementById(circleID);
            if (ready === true) {
                circle.setAttribute("fill", 'green');

            // change to green red for all online players
            } else {
                circle.setAttribute("fill", 'white');
            }
        });


        this.socket.on('showReadyBut', (readyGame) => {
            var readyBut = document.getElementById('button')
            if (readyGame) {
                readyBut.disabled = false;
                readyBut.innerHTML = 'Ready';
                readyBut.style.backgroundColor = 'green'
                readyBut.style.color = 'white'
                readyBut.style.width = '70px'
                // get the stop watch ready
                setupCountdown(this.socket)

            }
        });


        this.socket.on('gameReadyStage', (playerID) => {
            document.getElementById("clock").innerHTML = ''
            var readyBut = document.getElementById('button')
            readyBut.innerHTML = 'GAME STARTS :) ';
            readyBut.style.backgroundColor = 'Blue'
            readyBut.style.color = 'white'
            readyBut.style.width = '230px'
            readyBut.style.boxShadow = 'none'
            readyBut.disabled = true;
            if (this.playerID === playerID) {
                var bottom = document.getElementById('submit');
                bottom.innerHTML = 'Finish Game!';
                bottom.style.display = "inline";
                // var button = document.createElement('BUTTON');
                // var text = document.createTextNode('Finish Game!'); 
                // button.appendChild(text); 
                // console.log(button)
                // bottom.appendChild(button); 

            }
            setupCountUp()
        });


        this.socket.on('changeLastplayer', (playerID) => {
            if(this.playerID == playerID){
                var bottom = document.getElementById('submit');
                // bottom.disabled = true;
                bottom.style.display = 'none'
            }
        });

        this.socket.on('gameFinsh', () => {
            document.getElementById('submit').addEventListener('click',  () => {
                var subBut = document.getElementById('submit')
              
                // finsh the game
				if (this.subReady === false) {
                    this.socket.emit('gameFinished')
                } 
				
			});
        });


        this.socket.on('gameFinished', () => {
            var readyBut = document.getElementById('button')
            var subBut = document.getElementById('submit')
            var clock = document.getElementById('clock')
            readyBut.innerHTML = 'GAME OVER!'
            readyBut.style.backgroundColor = 'red'
            readyBut.style.color = 'white'
            subBut.style.display = 'none'
            clearInterval(countT);
            clock.innerHTML = 'Your challenge finished in ' + submitTime;

              
        });




        function setupCountdown (socket) {
            // Set the date we're counting down to
            var countDownDate = 30
            var now = 0
            // Update the count down every 1 second
            countT = setInterval(function() {
        
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                        
                // Output the result in an element with id="demo"
                document.getElementById("clock").innerHTML =  "Please click ready button for the game in " + distance + " s!!!";
                    
                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(countT);
                    document.getElementById("clock").innerHTML = "PAGE EXPIRED";
                    socket.emit('timeOutShut');
                }
                now = now + 1
        
            }, 1000);
        
        }



        function setupCountUp () {
            // Set the date we're counting down to
            var countUp = 0
            var now = 0
            // Update the count down every 1 second
            countT = setInterval(function() {
        
                // Find the distance between now and the count down date
                var distance = countUp + now;

                var hours = Math.floor(distance % (60*60*24)/(60*60));
                var minutes = Math.floor(distance % (60*60)/60);
                var seconds = Math.floor(distance % 60);
                        
                // Output the result in an element with id="demo"
                document.getElementById("clock").innerHTML =  hours + "h " + minutes + "m " + seconds + "s ";
                submitTime = document.getElementById("clock").innerHTML
                // If the count down is over, write some text 
                // if (distance < 0) {
                //     clearInterval(countGame);
                //     document.getElementById("clock").innerHTML = "PAGE EXPIRED";
                //     socket.emit('timeOutShut');
                // }
                now = now + 1
        
            }, 1000);
        
        }

    }

    clickReadyBut(colorRBut) {
        this.socket.emit('clickedRBut', colorRBut);
    }

  
}








