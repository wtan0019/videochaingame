const express = require('express');
const app = express();

var sock = require('socket.io');
const port = process.env.PORT || 5000
var server = app.listen(port, () => {
    console.log(`Listening on port ${port}...`)
});
var io = sock(server);


app.use(express.static(__dirname + '/public'));
app.use(express.json({ limit: '2mb'}));


class Player {
    constructor(playerID, chainPos, chainName){
        this.playerID = playerID;
        this.chainPos = chainPos;
        this.chainName = chainName;
        this.joined = false;
        this.link = null;
        this.ipAddr = null;
        this.socketID = null;
        this.readyBut = false;
    }
}


class Chain {
    constructor(numOfplayers, chainName){
        this.players = {};
        this.numOfplayers = numOfplayers;
        this.chainName = chainName;
        this.chainReady = false;

        var i;
        var playerID;
        var playerPos;
        for (i = 1; i <= numOfplayers; i++) {
            playerID = chainName.concat(String(i))
            // console.log(playerID)
            playerPos = String(i)
            var eachPlayer = new Player(playerID, playerPos, chainName);
            // this.players.push({playerID: playerID, playerObj: eachPlayer})
            // console.log(eachPlayer)
            this.players[playerID] = eachPlayer;

        } 
    }
}

class Game {
    constructor(numOfChains, lenOfChain) {
        this.numOfChains = numOfChains;
        this.lenOfChain = lenOfChain;
        this.start = false;
        this.chains = {};
        this.gameReady = false;

        var i;
        var chainName;
        for (i = 1; i <= numOfChains; i++) {
            chainName = String(i)
            // console.log(chainName)
            var eachChain = new Chain(lenOfChain, chainName);
            // this.chains.push({chainName: chainName, chainObj: eachChain})
            this.chains[chainName] = eachChain;

        } 
    }
}
 

function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 16;
	var randomstring = '';
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	return randomstring
}


function updateJoinedPlayer (numOfChains, lenOfChain) {
    var firstAllowPos = []
    for (i=1;i<=numOfChains;i++){
        for(j=1;j<=lenOfChain;j++){
            var k = String(i).concat(String(j))
            // console.log(game.chains[i].players[k].joined);
            if (game.chains[i].players[k].joined === false){
                firstAllowPos = {"chainName": i,"playerID" : k, "chainPos":j}
                return firstAllowPos;
            }
        }
    }
    return false;
}

function findJoinedPlayer (gameChain) {
    var joinedPos = [];
    var totalPlayers = gameChain.numOfplayers;
    var cid = String(gameChain.chainName);
    var pid;

    for(var i = 1;i<=totalPlayers;i++){
        pid = cid.concat(i)
        if (gameChain.players[pid].joined === true) {
            // console.log(pid)
            joinedPos.push(gameChain.players[pid].chainPos);
        }
    }
    return joinedPos;
}


function generateLRurl(firstAllowPos){
    var id = Number(firstAllowPos.playerID);
    var left = null;
    var right = null;
    var urlLR = [];
    if (firstAllowPos.chainPos === head){
        left = false;
        right = roomID + String(id);
    } else if (firstAllowPos.chainPos === tail){
        left = roomID + String(id - 1);
        right = false;
    } else {
        left = roomID + String(id - 1)
        right = roomID + String(id);
    }
    urlLR = {"left": left, "right": right}
    // console.log("first allow",firstAllowPos)
    // console.log(game.chains[firstAllowPos.chainName].players[firstAllowPos.playerID]);
    game.chains[firstAllowPos.chainName].players[firstAllowPos.playerID].link = urlLR;
    // console.log(game.chains[firstAllowPos.chainName]);
    return urlLR;
}

function findDisSocketID(socketID) {
    for (i=1;i<=numOfChains;i++){
        for(j=1;j<=lenOfChain;j++){
            var k = String(i).concat(String(j))
            // console.log(game.chains[i].players[k].joined);
            if (game.chains[i].players[k].socketID === socketID){
                disPos = {"chainName": i,"playerID" : k, "chainPos":j}
                return disPos;
          
            }
        }
    }
    return false;
}

function getOnlinePlayerID(chain){
    console.log(chain)
    var numPlayers = chain.numOfplayers;
    var chainName = chain.chainName;
    var playerID, playerIDlist = [];
    for(i = 1;i<=numPlayers;i++){
        playerID = String(chainName).concat(String(i))
        if (chain.players[playerID].joined === true){
            playerIDlist.push(playerID)
        }
    }
    return playerIDlist;
}

function checkGameReady(chain){
    var numPlayers = chain.numOfplayers;
    var chainName = chain.chainName;
    var playerID, playerIDlist = [];
    for(i = 1;i<=numPlayers;i++){
        playerID = String(chainName).concat(String(i))
        if (chain.players[playerID].joined === true){
            playerIDlist.push(playerID)
        }
    }
    if(playerIDlist.length === numPlayers){
        return true
    }
    return false
}


function resetReadyButState(chainName, playerOnlineIDs){
    for(i = 0;i<=playerOnlineIDs.length;i++){
        console.log(playerOnlineIDs[i])
        if (playerOnlineIDs[i]) {
            game.chains[chainName].players[playerOnlineIDs[i]].readyBut = false;
        }
    }
}


function checkChainReadyStates(chainName, playerOnlineIDs){
    var numOfReady = 0;
    for(i = 0;i<=playerOnlineIDs.length;i++){
        console.log(playerOnlineIDs[i])
        if (playerOnlineIDs[i]) {
            if (game.chains[chainName].players[playerOnlineIDs[i]].readyBut === true){
                numOfReady = numOfReady + 1
            }
        }
    }
    if (numOfReady === playerOnlineIDs.length) {
        return true
    }
    return false
}


// game define
var numOfChains = 1;
var lenOfChain =5;
var head = 1;
var tail = lenOfChain;
var numClients;
var roomID = randomString();
var startTime, endTime, timeDiff;
var gameDraw = true;

var circleWid = 35;
var cx=15, cy=15, r = 10;
var circleWidTotel = String(lenOfChain * circleWid) + "px";
var color = "#A9A9A9"

var posColor = "white";
var posR = 13;

var circleWid = 35;
var cx=15, cy=15, r = 10;

var playerOnlineIDs;
var gameReady=false;


function start(){
    startTime = new Date();
}

function end(){
    endTime = new Date();
    timeDiff = endTime - startTime;
    timeDiff /= 1000;
    // var seconds = Math.abs(timeDiff);
    console.log(timeDiff + "ms seconds");
    // console.log(seconds + " seconds");
}


var game = new Game(numOfChains, lenOfChain);
console.log("game restart", game.start)


start()

io.on('connection',  (socket) => {

    var joinedPos = [];
    console.log('\n----socket connecting for new connection or refresh on the browsers-----\n')
    console.log('This visitor IP is: ',socket.handshake.headers.host)
    console.log('This visitor addr is: ',socket.handshake.address)
    numClients = io.engine.clientsCount
    console.log('number of visitors: ', numClients)
    console.log('check 2', socket.connected)
    console.log(socket.id)
    console.log("game restart", game.start)
    console.log("numClients",numClients)

    end();

    if (gameDraw === true) {
        socket.emit("drawChain", circleWidTotel, lenOfChain, cx, cy, r, color)
  
    }

    if (game.start === false) {
        console.log("game.start === false", game.start)
        if (timeDiff < 10) {
            console.log("game restart in timeDiff < 10.0", game.start)
            socket.emit('restart', lenOfChain)
        } else {
            game.start = true
        }
    }
    

    firstAllowPos = updateJoinedPlayer (numOfChains, lenOfChain);
    // full chain condition
    if (firstAllowPos === false){
        // game.chains[firstAllowPos.chainName].chainReady = true;
        urlLR = "https://www.gstatic.com/webp/gallery3/2.png"
        console.log("it is full")
        socket.emit('geturl', urlLR, false)
        // console.log("heyhey",numClients)
    } else {
        game.chains[firstAllowPos.chainName].players[firstAllowPos.playerID].joined = true;
        urlLR = generateLRurl(firstAllowPos);
        console.log(urlLR)
        console.log(game.chains[1])
        joinedPos = findJoinedPlayer(game.chains[1]);
        console.log(joinedPos)
        socket.emit("updateCirclePos", firstAllowPos, posColor, posR);
        socket.emit('joined', firstAllowPos, socket.id, urlLR);
        socket.emit('geturl', urlLR, firstAllowPos);
        game.chains[firstAllowPos.chainName].players[firstAllowPos.playerID].socketID = socket.id;
        io.emit("updateCircleOn", joinedPos)
        console.log("upcircle after, before rect")
        socket.emit('updateRBut');
        gameReady = checkGameReady(game.chains[firstAllowPos.chainName])
        io.emit('showReadyBut', gameReady)
        // console.log(game.chains[firstAllowPos.chainName])
        io.emit('gameFinsh')
    }


    console.log(firstAllowPos)

  

    socket.on('disconnect', () => {
        console.log('\n+++++   socket disconnecting +++++++\n')
        console.log(firstAllowPos)
        console.log(socket.id)
        console.log(numClients)
        disPos = findDisSocketID(socket.id);
        console.log(disPos)
        // reset disconnect position values
        if (disPos != false){
            game.chains[disPos.chainName].players[disPos.playerID].joined = false;
            game.chains[disPos.chainName].players[disPos.playerID].link = null;
            game.chains[disPos.chainName].players[disPos.playerID].socketID = null;

            console.log(game.chains[disPos.chainName])
            playerOnlineIDs = getOnlinePlayerID(game.chains[disPos.chainName])

            console.log(playerOnlineIDs)
            io.emit("updateCircleOff", disPos, playerOnlineIDs)
            resetReadyButState(disPos.chainName, playerOnlineIDs)
            io.emit("updateReadyButOff", false)
            console.log(game.chains[disPos.chainName]) 
            io.emit("updateTextCounT")
            if (game.chains[disPos.chainName].chainReady){
                io.emit("changeLastplayer", playerOnlineIDs[playerOnlineIDs.length - 1])
                game.chains[disPos.chainName].chainReady = false;
                
            }
        }
       
    });

  
    socket.on('updatingCirCor', (ready, upStatePos, playerID, chainName) => {
        console.log(playerID)
        console.log(game.chains[chainName].players[playerID].playerID)
        console.log(game.chains[chainName].players[playerID].readyBut)

        game.chains[chainName].players[playerID].readyBut = ready;

        console.log(game.chains[chainName])
     
        io.emit("updatedCirCor", ready, upStatePos)
    });

     
    socket.on('checkChainReadyState', (chainName) => {
        playerOnlineIDs = getOnlinePlayerID(game.chains[chainName])
        if (playerOnlineIDs.length === game.chains[chainName].numOfplayers) {
            game.chains[chainName].chainReady = checkChainReadyStates(chainName, playerOnlineIDs)
            if (game.chains[chainName].chainReady) {
                io.emit('gameReadyStage', playerOnlineIDs[playerOnlineIDs.length-1])
            }
        }
    });



    socket.on('timeOutShut', () => {
        socket.emit('restart', lenOfChain)
    });


    socket.on('gameFinished', () => {
        io.emit('gameFinished')
    });


    // console.log(game)
    // console.log(game.chains)
    console.log(game.chains[1])
    console.log("outside",numClients)

});






